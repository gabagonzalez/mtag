import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArrayComponent } from './page/array/array.component';
import { TableComponent } from './page/table/table.component';

// routes
const routes: Routes = [
  { path: '' , redirectTo: 'array', pathMatch: 'full'},
  { path: 'array', component: ArrayComponent },
  { path: 'table', component: TableComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
