import { Component, OnInit } from '@angular/core';
import { ArrayService } from 'src/app/services/array.service';


@Component({
  selector: 'app-array',
  templateUrl: './array.component.html',
  styleUrls: ['./array.component.css']
})
export class ArrayComponent implements OnInit {
  private originArray:Array<number>;
  private listArray: Array<any>;
  private sortedArray: Array<any>;
  private defaultArray:Array<any>;
  private errorArray:Array<any>;
  

  constructor( private arrayService: ArrayService) { 
  
    this.originArray=[];
    this.listArray=['loading...'];
    this.sortedArray=['loading...'];
    this.defaultArray=[];
    this.errorArray=[];

 
  }

  ngOnInit() {
  }

  getAll(){  
    // 1- Data from Service
    this.arrayService.getData().subscribe(res => {
      console.log(res);
      
      //2- Get Info (OriginArray-)
      if (res['success']){
        res['data'].forEach(element => {
          this.originArray.push(element);
        });
        //3-For Texbox
        this.listArray= this.originArray;   
        console.log(this.originArray);
        //4- Array Sort         
        this.sortedArray = this.sorted(this.originArray);
          console.log(this.sortedArray);
        //5-For Array
        this.defaultArray = this.breaking(this.originArray);
      }else{
        //6- Data error
        this.defaultArray=[{'numb': '--', 'frec': '--', 'first_pos': '--', 'last_pos':'--' }];
        this.listArray = ['data error'];
        this.sortedArray=['press the button again to get result'];
        console.log('data error');
      }
    });
  }

  // Complementary functions
  //4- Array Sort 
  sorted(res:any){
    let arr = res.slice();
    for( let i=0;i<arr.length;i++){ 
      for(let j=0;j<arr.length-1;j++){
        if(arr[j] > arr[j+1]){
          [arr[j], arr[j+1]] =[arr[j+1],arr[j]];
        }
      }
    }
    return arr;
  }

  // 5-
  breaking(res:any){
    let arr = res;
    let objArray = [];
      for (let i=0; i < arr.length; i++){
        let count = {};
        let condition = true;
        for (let j =0; j<objArray.length; j++){
          let aux = Object.entries(objArray[j]);
            if(aux[0][1] === arr[i]){
            count = objArray[j];
            objArray.splice(j,1);
            condition = false;
          }else{
            condition=true;
          }
          if (!condition) break;        
        }
        if (condition){ 
          count['numb'] = arr[i];
          count['frec'] = 1;
          count['first_pos'] = arr.indexOf(arr[i]);
          count['last_pos'] = arr.lastIndexOf(arr[i]);        
        }else{ 
          count['frec'] += 1;
        }
        objArray.push(count);       
      }
      
      objArray.sort(function(a,b){
        if (a.first_pos > b.first_pos) return 1;
        if (a.first_pos < b.first_pos) return -1;
        return 0;
      });
  
      return objArray;
    }


}
