import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {IArray} from '../interface/Array.interface';
 

@Injectable({
  providedIn: 'root'
})
export class ArrayService {
  

  private api = 'http://patovega.com/prueba_frontend/array.php';

  constructor(private http: HttpClient) { }
  
  // Get info Data
  getData(){
    const url=`${this.api}`;
    return this.http.get<IArray[]>(url);    
  }
  
  
}
   
  

